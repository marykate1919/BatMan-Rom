#!/sbin/sh
# Written by Tkkg1994

aromabuildprop=/tmp/aroma/buildtweaks.prop
buildprop=/system/build.prop

getprop ro.boot.bootloader >> /tmp/BLmodel

mount /dev/block/platform/11120000.ufs/by-name/SYSTEM /system

if grep -q finger=1 $aromabuildprop; then
	echo "# Fingerprint Tweak" >> $buildprop
	echo "fingerprint.unlock=1" >> $buildprop
fi

if grep -q user=1 $aromabuildprop; then
	echo "# Multiuser Tweaks" >> $buildprop
	echo "fw.max_users=30" >> $buildprop
	echo "fw.show_multiuserui=1" >> $buildprop
	echo "fw.show_hidden_users=1" >> $buildprop
	echo "fw.power_user_switcher=1" >> $buildprop
fi

if grep -q fix=1 $aromabuildprop; then
	echo "# Screen mirror fix" >> $buildprop
	echo "wlan.wfd.hdcp=disable" >> $buildprop
fi

if grep -q G950 /tmp/BLmodel; then
	echo "# dream Model" >> $buildprop
	echo "batman.id=dream" >> $buildprop
else if grep -q G955 /tmp/BLmodel; then
	echo "# dream2 Model" >> $buildprop
	echo "batman.id=dream2" >> $buildprop
else
	echo "Unsupported model" >> $buildprop
fi
fi

exit 10
